import os
import requests
import json
from helpers import *

from flask import Flask, session, render_template, request, redirect, url_for, Markup
from flask_session import Session
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker



app = Flask(__name__)

# Check for environment variable
if not os.getenv("DATABASE_URL"):
    raise RuntimeError("DATABASE_URL is not set")

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Set up database
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    username=session.get('username')
    session["books"] = []
    message='List of books'
    default_data = db.execute("SELECT * FROM books").fetchall()
    for item in default_data:
        session["books"].append(item)
    if request.method == "POST":
        session["books"] = []
        text=request.form.get('text') 
        data = db.execute("SELECT * FROM books WHERE author iLIKE '%"+text+"%' OR title iLIKE '%"+text+"%' OR isbn iLIKE '%"+text+"%'").fetchall()
        for item in data:
            session["books"].append(item)
        if len(session["books"])==0:
            message="Nothing found. Try again."
    return render_template("index.html",data=session["books"],message=message,username=username)

@app.route("/isbn/<string:isbn>", methods=["GET", "POST"])
def book(isbn):
    session["reviews"] = []
    username = session.get('username')
    clone_review = db.execute("SELECT * FROM reviews WHERE isbn = :isbn AND username = :username", {"username":username, "isbn":isbn}).fetchone()
    warning = ""
    res = requests.get("https://www.goodreads.com/book/review_counts.json", params={"key": "Cdjuz7jTYIwy5Jj9GhY9sw", "isbns": isbn})
    average_rating=res.json()['books'][0]['average_rating']
    work_ratings_count=res.json()['books'][0]['work_ratings_count']
    data=db.execute("SELECT * FROM books WHERE isbn = :isbn",{"isbn":isbn}).fetchone()
    if username != None:
        if request.method == "POST" and clone_review == None:
            review = request.form.get("textarea")
            rating = request.form.get("stars")
            db.execute("INSERT INTO reviews (isbn, review,rating,username) VALUES (:isbn, :review, :rating, :username)",{"isbn":isbn, "review":review, "rating":rating, "username":username})
        if request.method == "POST" and clone_review != None:
            warning = "Sorry, you can`t add a second review!"
    elif request.method == "POST" and username == None:
        return  render_template("login.html", log_in_message = "Login to write a reviews!")
    reviews=db.execute("SELECT * FROM reviews WHERE isbn = :isbn",{"isbn":isbn}).fetchall() 
    for y in reviews:
        session['reviews'].append(y) 
    return render_template("book.html", data=data,reviews=session['reviews'],average_rating=average_rating,work_ratings_count=work_ratings_count,username=username,warning=warning)

@app.route("/login", methods=["GET", "POST"])
def login():
    if session.get('username') != None:
        messege = "You already logged in"
        return redirect(url_for('index'))
    log_in_message = ""
    if request.method=="POST":
        emailLogIn=request.form.get('emailLogIn') 
        userPasswordLogIn=request.form.get('userPasswordLogIn')                                 ## registration
        data=db.execute("SELECT * FROM users WHERE username = :a",{"a":emailLogIn}).fetchone()
        if data!=None:
            if data.username==emailLogIn and data.password==userPasswordLogIn:
                session["username"]=emailLogIn
                return redirect(url_for("index"))
            else:
                log_in_message="Wrong email or password. Try again."
        else:
            log_in_message="Wrong email or password. Try again."
    return render_template('login.html',log_in_message=log_in_message, username=session.get('username'))

@app.route("/register", methods=["GET", "POST"])
def register():
    if session.get('username') != None:
        messege = "You already logged in"
        return redirect(url_for('index'))
    log_in_message = ""
    if request.method=="POST":
        email=request.form.get('email') 
        userPassword=request.form.get('userPassword')
        data=db.execute("SELECT username FROM users").fetchall()
        for i in range(len(data)):
            if data[i]["username"]==email:
                log_in_message="Sorry. Username already exist"
                return render_template('register.html', log_in_message=log_in_message, username=session.get('username'))
        db.execute("INSERT INTO users (username,password) VALUES (:a,:b)",{"a":email,"b":userPassword})
        db.commit()
        log_in_message="Success!"
        session['username'] = email
        return redirect(url_for('index'))
    return render_template('register.html', log_in_message=log_in_message, username=session.get('username'))
                                                       


@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("login"))
    

