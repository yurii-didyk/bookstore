from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String, nullable = False)
    password = db.Column(db.String, nullable = False)

class Review(db.Model):
    __tablename__ = "review"
    id = db.Column(db.Integer, primary_key = True)
    review = db.Column(db.String, nullable = False)
    rating = db.Column(db.Integer, nullable = False)
    isbn = db.Column(db.String, nullable = False)

class Book(db.Model):
    __tablename__ = "books"
    isbn = db.Column(db.String, primary_key = True)
    title = db.Column(db.String, nullable = False)
    author = db.Column(db.String, nullable = False)
    year = db.Column(db.String, nullable = False)
